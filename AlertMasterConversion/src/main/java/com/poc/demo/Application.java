package com.poc.demo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.poc.demo.domain.AlertMaster;
import com.poc.demo.domain.AlertMasterProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import java.util.*;

/**
 * Created by cj on 7/20/17.
 */
public class Application {
    public static void main (String args[]) {

        String jsonString = "{\"alertmasters\": [{ \"securitydelegate\":{\"alertName\":\"abcd\", \"isChildAlert\":\"false\", \"eventType\":\"something\"}, \"wealth_create_delegate\":{\"alertName\":\"abcd2\", \"isChildAlert\":\"true\", \"eventType\":\"something2\"} }]}";
        HashMap<String, ArrayList<HashMap<String, AlertMasterProperties>>> alertMasterMap = new Gson().fromJson(jsonString, new TypeToken<HashMap<String, List<HashMap<String, AlertMasterProperties>>>>() {
        }.getType());
        List<HashMap<String, AlertMasterProperties>> internalValueList = null;
        System.out.println(alertMasterMap.get("alertmasters"));

        for (Map.Entry<String, ArrayList<HashMap<String, AlertMasterProperties>>> entry : alertMasterMap.entrySet()) {

            String key = entry.getKey();
            internalValueList = entry.getValue();
            System.out.println("-------");
            System.out.println(key);
            System.out.println(internalValueList);
            System.out.println("-------");

        }

        System.out.println("----Internal---");
            for (HashMap<String, AlertMasterProperties> key : internalValueList) {
                Collection<AlertMasterProperties> ampc = key.values();
                for (AlertMasterProperties amp: ampc) {
                    System.out.println(amp.getAlertName());
                    System.out.println(amp.getEventType());
                    System.out.println(amp.getIsChildAlert());
                }

            }
        System.out.println("---Internal----");



    }

}
