package com.poc.demo.domain;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by cj on 7/20/17.
 */
public class AlertMaster{

    @SerializedName("alertmasters")
    private List<HashMap<String, AlertMasterProperties>> alertMasterList = new ArrayList<HashMap<String, AlertMasterProperties>>();


    public List<HashMap<String, AlertMasterProperties>> getAlertMasterList() {
        return alertMasterList;
    }

    public void setAlertMasterList(List<HashMap<String, AlertMasterProperties>> alertMasterList) {
        this.alertMasterList = alertMasterList;
    }
}
