package com.poc.demo.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cj on 7/20/17.
 */
public class AlertMasterProperties implements Serializable{
    @SerializedName("alertName")
    String alertName = "";
    @SerializedName("eventType")
    String eventType = "";
    @SerializedName("isChildAlert")
    String isChildAlert = "";

    public String getAlertName() {
        return alertName;
    }

    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getIsChildAlert() {
        return isChildAlert;
    }

    public void setIsChildAlert(String isChildAlert) {
        this.isChildAlert = isChildAlert;
    }
}
